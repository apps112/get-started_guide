Create and set up team
======================
Organizations are an easy way of organizing and bundling teams working on the same tasks. 

Your company could be divided into several departments, branches, or teams. For instance, if you own 3 coffee shops located in 3 different cities, each coffee shop will have its own manager who oversees virtual staffs. As the owner, you can invite the branch managers of the coffee shops to each manage their accounts (in this case, coffee shops) separately. As the manager, you get to view each coffee shop with its own stats on your dashboard. 
Setting up a new organization is very simple, and you can have an unlimited number of organizations in your account.

Step 1, click on the Organizations tab on the left sidebar.

 .. image:: dashboard-organization.png
  :align: center

Step 2, locate the button to add an organization.

 .. image:: organization-button.png
     :align: center
     
Step 3, fill in the details of your organization on the pop-up form and click create!click save

 .. image:: create-organization.png
  :align: center

