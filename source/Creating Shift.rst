Creating Shift
==============
In this section, we guide you on how to schedule work shifts for your team. 
As an admin, you can easily schedule team tasks and automatically monitor the attendance of your team. 
Easily create recurring or one-time shifts, view schedules for your entire team or individual members, and view the shift status of clocked-in members. 
Start by clicking the Schedules tab on the left sidebar.


.. image:: dashboard-shift.png
  :align: center

Here you can navigate to previous or future weeks using the navigation arrows.  

.. image:: shift-filter.png
  :align: center

By default, all users on your team will be shown here if they have scheduled tasks. 

Click the Create Shift tab on the left sidebar to create a new shift.


.. image:: create-shift.png
  :align: center


Fill in the details below:

   #. Shift name
   #. Select an employee or multiple employees
   #. Fill in the shift duration to include start and end dates including the start and end times
   #. Enter the minimum hours of the shift
   #. Select location of the shift 
   #. Choose between repeat and no repeat shift patterns. 
   #. For Repeat Shift Pattern option, enter the shift repeat until date
   #. Select the days the shift will repeat
   #. Click on Create to save changes.


After creating a shift, you will see the schedule plotted on the Shifts screen


.. image:: view-shift.PNG
  :align: center


Deleting Shifts
You can delete a shift by clicking on the shift you created under Shifts. After clicking on the shift, you will receive a popup window where you can Delete or Edit the schedule. Click on Delete to remove the schedule.


