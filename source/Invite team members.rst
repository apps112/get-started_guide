Invite team members
===================
Invite team members to your workspace. When you invite teams, they will receive a mail to join the existing workspace.

You can invite a single team member or import your organization’s bulk employee data.

Add members to your organization with these three easy steps.

#. Step 1
Click on the Members tab on the left sidebar.
                .. image:: dashboard-members.png
                  :align: center

#. Step 2
Click on the Add Member button.
Equally select the import member’s button.
Note: The import feature needs your file to be in the specified format.

                        .. image:: team-button.png
                          :align: center

#. Step 3
Fill in the details and click done

.. image:: create-team.png
  :align: center

