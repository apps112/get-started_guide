Timesheet
=========
In this section, we guide you on how to view the timesheet generated from the shifts worked by your team. 
As an admin, you can easily view the timesheets of your employees 

Start by clicking the Timesheets tab on the left sidebar.
Here you can navigate to previous or future weeks using the navigation arrows. Using the calendar dropdown, you can easily specify the timelines you want to view. 

.. image:: view-timesheets.PNG
  :align: center

Exporting and Downloading Timesheet Data by clicking on the export button. 



