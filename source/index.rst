.. image:: logo.png
	:align: center


Getting Started with Gigsasa!
=============================

Welcome to the GigSasa Family! 
We’re happy to have you aboard one of the revolutionary platforms for scheduling your team jobs and tasks, tracking your teams time and activity, and performance measure analytics. GigSasa offers the ability to monitor how and when you team check in for their jobs, how much time they spend on average working, and more. 
It can be hard to learn a new platform, but we got you! This guide will provide you with all the information you need to successfully optimize GigSasa for your business.


Contents:
=========

.. toctree::
   :maxdepth: 1
   
   Getting Started
   Create and set up your team
   Invite team members
   Create Job Sites
   Creating Shift
   Timesheet Management
   Monitor and track productivity
   Manage your account
 



Links:
======

* **Web Portal**
	* `Website <https://gigsasa.com/>`_
