Manage your account
===================
This guide will provide all the information to succeed in optimizing your business. 


Start by clicking the General tab on the left sidebar and fill in the options you wish to change and click on save. Admins can toggle the performance questions and facial recognition depending on the usage of these features.

.. image:: dashboard-settings.png
  :align: center

.. image:: company-settings.png
  :align: center


